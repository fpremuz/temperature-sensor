#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSans18pt7b.h>
#include <WiFi.h>
#include <PubSubClient.h>  
#include <DNSServer.h>
#include <WebServer.h>
#include <WiFiManager.h>   

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

// GPIO where the DS18B20 is connected to
const int oneWireBus = 4;

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(oneWireBus);

// Pass our oneWire reference to Dallas Temperature sensor
DallasTemperature sensors(&oneWire);

//MQTT config
const char *broker = "test.mosquitto.org"; 
const char *root_topic_subscribe = "subscriber";
const char *root_topic_publish = "Sensor/Temperatura";

WiFiClient espClient;
PubSubClient client(espClient);
long currentTime, lastTime;
long lastMsg = 0;
char msg[25];
long count = 0;

// Callback function header
void callback(char *topic, byte *payload, unsigned int length);

void reconnect()
{
  while (!client.connected())
  {
    Serial.print("Conectando a broker: ");
    Serial.println(broker);
    if (client.connect("Sensor")) //, brokerUser, brokerPass))
    {
      Serial.print("Te conectaste a: ");
      Serial.println(broker);
      client.subscribe(root_topic_subscribe);
    }
    else
    {
      Serial.println("Tratando de reconectarse");
      delay(5000);
    }
  }
}

void configModeCallback (WiFiManager *myWiFiManager) {
 Serial.println("Modo de configuración ingresado");
 Serial.println(WiFi.softAPIP()); 
 Serial.println(myWiFiManager->getConfigPortalSSID());
}
 
bool shouldSaveConfig = false;
 
void saveConfigCallback () {
 Serial.println("Debería guardar la configuración");
 shouldSaveConfig = true;
}

void setup()
{
  Serial.begin(115200);
  sensors.begin();
  WiFiManager wifiManager;

  if(!wifiManager.autoConnect("WiFi Sensor de temperatura", "sensorunsam")) {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
  } 


//   wifiManager.setAPCallback(configModeCallback);
//   wifiManager.setSaveConfigCallback(saveConfigCallback); 
//   wifiManager.autoConnect("WiFi Sensor de temperatura", "sensorunsam");

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;
  }
  delay(2000); // tiempo para que inicialice el display
  display.clearDisplay();
  display.setTextColor(WHITE);

  Serial.println("Conectándose al WiFi");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print(".");
  }
//   Serial.println();
//   Serial.println("WiFi conectado IP: ");
//   Serial.println(WiFi.localIP());

  //MQTT
  client.setServer(broker, 1883);
  client.setCallback(callback);
}

void loop()
{
  if (!client.connected())
  {
    reconnect();
  }
  client.loop();

//   WiFiManager wifiManager;
//   if(!wifiManager.startConfigPortal("WiFi Sensor de temperatura", "sensorunsam") ){
//    Serial.println("No se pudo conectar");
//    delay(2000);
//    ESP.restart();
//    delay(1000);
//   }
//   Serial.println("conectado al WiFi Sensor de temperatura");

  long now = millis();
  if (now - lastMsg >= 1000)//1000 = 1 segundo entre cada mensaje
  { 
    lastMsg = now;
    //sensors.setResolution(12);
    sensors.requestTemperatures();
    float temperature = sensors.getTempCByIndex(0);
    delay(1000);

    display.clearDisplay();
    display.setFont(&FreeSans9pt7b);
    display.setCursor(0, 20); //posicion
    display.print("Temperatura: ");
    display.setFont(&FreeSans18pt7b);
    display.setTextSize(1);
    display.setCursor(0, 60);
    display.print(temperature);
    display.print("  ");
    display.drawCircle(100, 45, 2, WHITE); //dibuja "º"
    display.setFont(&FreeSans12pt7b);
    display.print("C");
    display.display();

    // Convert the value to a char array
    char tempString[8];
    dtostrf(temperature, 1, 2, tempString);
    Serial.print("Temperatura: ");
    Serial.print(temperature);
    Serial.print("ºC\n");
    //Serial.println(tempString);
    client.publish(root_topic_publish, tempString);//publico el valor de la temperatura en en topic Temperatura
  }
}

void callback(char *topic, byte *payload, unsigned int length)
{
  String incoming = "";
  Serial.print("Mensaje recibido desde: ");
  Serial.print(topic);
  Serial.println("");
  for (int i = 0; i < length; i++)
  {
    incoming += (char)payload[i];
  }
}


//PROBAR CON EL CODIGO VIEJO, CONSEGUIR SSID CON LA FUNCION