import datetime
import time
import json
import paho.mqtt.client as mqtt
import sys
from pymongo import MongoClient
from bson import json_util
from datetime import date
from datetime import datetime

def crear_db(db_name):
    MQTT_Broker = "test.mosquitto.org"	
    MQTT_Port = 1883		
    Keep_Alive_Interval = 45
    MQTT_Topic = "Sensor/Temperatura" 

    client = MongoClient()		
    db = client.temp_database
    mqtt_collection = db[db_name]

    def on_connect(mqttc, mosq, obj, rc):
        mqttc.subscribe(MQTT_Topic, 0)

    def on_message(mosq, obj, msg):
        today = date.today()
        d1 = today.strftime("%Y/%m/%d") 
        now = datetime.now()
        dt_string = now.strftime("%H:%M:%S") 
        medicion = {
            "Temperatura": msg.payload.decode('utf-8'),
            "Fecha y hora": d1 + " " + dt_string,
        }
        stored_med = mqtt_collection.insert_one(medicion)
    
        cursor = mqtt_collection.find()

    def on_subscribe(mosq, obj, mid, granted_qos):
        pass
    
    mqttc = mqtt.Client()
    mqttc.on_connect = on_connect
    mqttc.on_message = on_message
    mqttc.subscribe("#")
    mqttc.connect(MQTT_Broker, int(MQTT_Port), int(Keep_Alive_Interval))
    # mqttc.loop_forever()
    mqttc.loop_start()
    time.sleep(2)
    mqttc.loop_stop()

    return 