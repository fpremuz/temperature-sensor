import dash
import dash_daq as daq
import dash_auth
from dash.dependencies import Output, Input, State
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
from dash_extensions import Download
from datetime import datetime, timezone
from datetime import timedelta
import time
import plotly
import plotly.graph_objs as go
from collections import deque
import plotly.express as px
import plotly.graph_objs as go
import numpy as np
import pandas as pd

from mqttsub import get_temp
from showdbs import show_dbs
from mongofind import ejes_grafico
from delete_db import borrar_database
from button_enable import boton, estado, db_en_uso, db_actual
from copy_db import create_db_in, create_db_fin
from iot_graph import graph_mqtt

interval = 1000

mqtt_time=[]
mqtt_temp=[]
time_min=[]

# the style arguments for the sidebar.
SIDEBAR_STYLE = {
    'position': 'fixed',
    'top': 0,
    'left': 0,
    'bottom': 0,
    'width': '20%',
    'padding': '20px 10px',
    'background-color': '#f8f9fa'
}

# the style arguments for the main content page.
CONTENT_STYLE = {
    'margin-left': '25%',
    'margin-right': '5%',
    'padding': '20px 10p'
}

TEXT_STYLE = {
    'textAlign': 'center',
    'color': '#191970'
}

controls = dbc.FormGroup(   #sidebar
    [
        html.P('Bases de datos', style={
            'textAlign': 'center'
        }),
        html.Div([
            dcc.Dropdown(
                id='dropdown',
                value=None,   
                placeholder="Graficar base de datos",
                multi = True
            ),
            dcc.Interval(
                id='interval-component-dropdown',
                interval=interval, # in milliseconds
                n_intervals=0
            ),
        ]),
        html.Br(),
        html.Div([
            dcc.Input(id='nombre_db',placeholder = 'Nombre de la base de datos',size='35' , type='text'),
            dbc.Button(
                id='submit_button',
                type='submit',
                n_clicks=0,
                children='Comenzar a guardar datos',
                color='primary',
                block=True
            ),
            dcc.Interval(
                id='interval-flag',
                interval=interval, # in milliseconds
                n_intervals=0
            ), 
            html.Div(id='create_db'),
            html.Br(),
            dbc.Button(
                id='boton_fin_db',
                type='submit',
                n_clicks=0,
                children='Finalizar guardado de datos',
                color='primary',
                block=True
            ),
            html.Div(id='fin_db')
        ]),
        html.Br(),
        html.Div([
            dcc.Dropdown(
                id='dpdown_download',
                value=None,   
                placeholder="Seleccione base de datos a descargar"
            ),
            dcc.Interval(
                id='interval_download',
                interval=interval, # in milliseconds
                n_intervals=0
            ),
            dbc.Button(
                "Descargar base de datos",
                id='descargar',
                color='secondary',
            ),
            dcc.Download(id='download_db')
        ]),
        html.Br(),
        html.Div(id='delete'),
        dcc.Dropdown(
                id='dropdown_borrar',
                value=None,   
                placeholder="Seleccione base de datos a borrar"
            ),
        dcc.Interval(
                id='interval-borrar_db-dropdown',
                interval=interval, # in milliseconds
                n_intervals=0
            ),
        dbc.Button(
            id='borrar_db',
            n_clicks=0,
            children='Borrar base de datos',
            color='danger',
            block=True
        ),
        html.Br(),
        html.Br(),
        html.P('Tomar muestras cada: [minutos]'),
        html.Div(id='frec'),
        dcc.Input(id='frec_muestreo',placeholder = 'Cuántos minutos hay entre cada muestra ',size='35' , type='number', value=1, min=0),
        html.Div(id='frec_seg'),
    ]
)

sidebar = html.Div(
    [
        html.H1('Funciones', style=TEXT_STYLE),
        html.Hr(),
        controls,
    ],
    style=SIDEBAR_STYLE,
)

content_first_row = html.Div(children=[
    html.Div([
        html.Div([
            html.Div(id='live-update-text-temp', style={'fontSize': 20}),
            dcc.Interval(
                id='interval-component-temp',
                interval=interval*3, # in milliseconds
                n_intervals=0
            ) 
        ])
    ])
])

content_second_row = html.Div(children=[
    html.Div([
        html.Div([
            html.Div(id='live-update-text-hora', style={'fontSize': 20}),
            dcc.Interval(
                id='interval-component-hora',
                interval=interval, # in milliseconds
                n_intervals=0
            ), 
            html.Div(id='live-update-text-fecha', style={'fontSize': 20}),
            dcc.Interval(
                id='interval-component-fecha',
                interval=interval, # in milliseconds
                n_intervals=0
            ),
        ]),

        html.Div([
            daq.Thermometer(
                id='my-thermometer',
                value=0,
                min=0,
                max=30,
                style={
                    'margin-bottom': '5%',
                    'margin-left': '65%',
                }
            ),
        ]),
    ])
])

content_third_row = html.Div(children=[
    html.Div([
        html.Div([
            html.Div(id='graph_timespan'),
        ]),
        html.Div([
            dcc.Input(id='timespan',placeholder = 'Rango de tiempo del gráfico en tiempo real',size='40' , type='number', value=24, min=0)
        ]),
    ])
])

content_fourth_row = html.Div(children=[
        html.Div([
            dcc.Graph(
                id='live-graph_mqtt',
            ),
            dcc.Interval(
                id='graph_mqtt-update',
                interval=interval*1,
                n_intervals = 0
            ),  
    ])
])

content_fifth_row = html.Div([
    html.Div(id='graficar_dropdown'),
    dcc.Interval(
        id='interval-grafico-dropdown',
        interval=interval*5, # in milliseconds
        n_intervals=0
    ), 
])


content = html.Div(
    [
        html.H1('Sensor de temperatura', style=TEXT_STYLE),
        html.Hr(),
        content_first_row,
        content_second_row,
        content_third_row,
        content_fourth_row,
        content_fifth_row,
    ],
    style=CONTENT_STYLE
)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])

auth = dash_auth.BasicAuth(
    app,
    {'sensorunsam':'sensorunsam'}
)

app.layout = html.Div([sidebar, content])

@app.callback(Output('live-update-text-hora', 'children'),
              Input('interval-component-hora', 'n_intervals'))

def update_hora(n):
    now = datetime.now()
    hora = 'Hora: ' + str(now.strftime("%H:%M:%S"))
    return hora

@app.callback(Output('live-update-text-temp', 'children'),
              Input('interval-component-temp', 'n_intervals'))

def update_temp(n):
    temp = get_temp()
    temperatura = 'Temperatura: ' + str(temp) + ' ºC'
    return temperatura

@app.callback(Output('live-update-text-fecha', 'children'),
              Input('interval-component-fecha', 'n_intervals'))

def update_fecha(n):
    now = datetime.now()
    fecha = 'Fecha: ' + str(now.strftime("%Y/%m/%d"))
    return fecha

@app.callback(dash.dependencies.Output('my-thermometer', 'value'),
    [dash.dependencies.Input('interval-component-temp', 'n_intervals')])

def update_thermometer(n):
    temp = float(get_temp())
    return temp

@app.callback(Output('graph_timespan', 'children'),
              Input('interval-component-temp', 'n_intervals'),
              State('timespan', 'value'))

def update_grafico_mqtt(n, value):
    if type(value) != float and type(value) !=int and value is None:
        raise PreventUpdate
    texto = 'Rango de tiempo del gráfico en tiempo real [horas]'
    return texto

@app.callback(Output('live-graph_mqtt', 'figure'),
              Input('graph_mqtt-update', 'n_intervals'),
              State('timespan', 'value'),
              State('frec_muestreo', 'value'))

def update_graph_mqtt(n, value, frec):

    if type(value) != float and type(value) != int and value is None and frec is None:
        raise PreventUpdate
   
    mqtt_time, mqtt_temp = graph_mqtt(value, frec)
    
    data = plotly.graph_objs.Scatter(
        x=mqtt_time,
        y=mqtt_temp,
        name='Scatter',
        mode='lines+markers'
    )

    if value >=1:
        title = str('Temperatura en función del tiempo de las últimas {} horas'.format(value))
    else:
        title = str('Temperatura en función del tiempo de los últimos {:0.2f} minutos'.format(value*60))

    return {
        'data': [data],
        'layout':{
        'title': title, 
        'yaxis':{'title':'Temperatura [ºC]'},
        'margin': {
                        'l': 60, # left margin, in px
                        'r': 40, # right margin, in px
                        't': 40, # top margin, in px
                        'b': 150, # bottom margin, in px
            }
        }
    }

@app.callback(
    Output('dropdown', 'options'),
    [Input('interval-component-dropdown', 'n_intervals')])
    
def update_options(n):
    db_list = show_dbs()
    existing_options = []
    for db in db_list:
        existing_options.append({'label': db, 'value': db})
    return existing_options

@app.callback(
    Output('graficar_dropdown', 'children'),
    [Input('interval-grafico-dropdown', 'n_intervals')],
    [Input('dropdown', 'value')],
    State('frec_muestreo', 'value'))

def name_to_figure(n, db_name, frec):
    graphs = []
    if db_name is not None:
        for values in db_name:
            x,y = ejes_grafico(values, frec)
            if frec < 1:
                titulo_x = 'Tiempo [segundos]'
            elif 1 <= frec < 30:
                titulo_x = 'Tiempo [minutos]'
            elif frec >= 30:
                titulo_x = 'Tiempo [horas]'
            graphs.append(dcc.Graph(
            id='graph-{}'.format(values),
            figure={
                'data': [{
                    'x': x,
                    'y': y
                }],
            'layout': {
                'title': 'Gráfico de la base de datos "{}"'.format(values),
                'xaxis':{'title':titulo_x},
                'yaxis':{'title':'Temperatura [ºC]'},
                'margin': {
                        'l': 60, # left margin, in px
                        'r': 40, # right margin, in px
                        't': 40, # top margin, in px
                        'b': 150, # bottom margin, in px
                    }
                }
            }
        ))
    return html.Div(graphs)

@app.callback(Output('create_db', 'children'),
                  [Input('submit_button', 'n_clicks')],
                  [Input('interval-flag', 'n_intervals')])

def crear_base_de_datos(n_clicks, n):
    nombre = db_actual()
    boton_estado = estado()
    if boton_estado: 
        msg = 'La base de datos "{}" ahora está en uso'.format(nombre)
    if not boton_estado:
        msg = 'No hay base de datos en uso'
    return html.Div(msg)

@app.callback(
    Output('submit_button', 'disabled'),
    [Output('boton_fin_db', 'disabled')],
    [Input('submit_button', 'n_clicks')],
    [Input('boton_fin_db', 'n_clicks')],
    [State('nombre_db', 'value')])

def stop(click_inicio, click_fin, value):
    changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if 'boton_fin_db' in changed_id:
        boton(False)
        create_db_fin()
    if 'submit_button' in changed_id:
        if value is not None:
            boton(True)
            db_en_uso(value)
            create_db_in()
    boton_estado = estado()
    if boton_estado == True:
        boton_fin = False
    if boton_estado == False:
        boton_fin = True
    return boton_estado, boton_fin

@app.callback(
    Output("download_db", "data"),
    Input("descargar", "n_clicks"),
    State('frec_muestreo', 'value'),
    [State('dpdown_download', 'value')],
    prevent_initial_call=True,)

def descargar_db(n_clicks, frec ,option):
    if option is None:
        raise PreventUpdate
    if option is not None:
        x,y = ejes_grafico(option, frec)
        if frec < 1:
            df = pd.DataFrame({"                   ":"          ", "Tiempo[segundos]": x, "         ":"      " , "Temperatura": y})
        elif 1 <= frec < 30:
            df = pd.DataFrame({"                   ":"          ", "Tiempo[minutos]": x, "         ":"      " , "Temperatura": y})
        elif frec > 30:            
            df = pd.DataFrame({"                   ":"          ", "Tiempo[horas]": x, "         ":"      " , "Temperatura": y})
    return dcc.send_data_frame(df.to_csv, f"{option}.csv")

@app.callback(
    Output('dpdown_download', 'options'),
    [Input('interval-component-dropdown', 'n_intervals')])
    
def update_options(n):
    db_list = show_dbs()
    existing_options = []
    for db in db_list:
        existing_options.append({'label': db, 'value': db})
    return existing_options

@app.callback(
    Output('dropdown_borrar', 'options'),
    [Input('interval_download', 'n_intervals')])
    
def update_options(n):
    db_list = show_dbs()
    existing_options = []
    for db in db_list:
        existing_options.append({'label': db, 'value': db})
    return existing_options

@app.callback(
    Output('delete', 'children'),
    [Input('borrar_db', 'n_clicks')],
    [State('dropdown_borrar', 'value')])

def delete(n_clicks, value):
    borrar_database(str(value))
    return 

@app.callback(Output('frec', 'children'),
              Input('interval-component-temp', 'n_intervals'),
              State('frec_muestreo', 'value'))

def update_frec_muestreo(n, value):
    return 

@app.callback(Output('frec_seg', 'children'),
              Input('interval-component-temp', 'n_intervals'),
              State('frec_muestreo', 'value'))

def update_frec_muestreo_seg(n, value):
    if type(value) != float and type(value) != int and value is None:
        msg = 'El muestreo no puede ser negativo'
    elif 0 <= value < 0.02:    
        msg = 'El muestreo no puede ser menor a 0.02 minutos \r Se adopta el mínimo (1 segundo)'
    elif value <= 1:
        seg = value * 60
        msg = 'Equivalen a {:0.2f} segundos'.format(seg)
    else:
        seg = value / 60
        msg = 'Equivalen a {:0.2f} horas'.format(seg)
    return msg

if __name__ == '__main__':
    # app.run_server(debug=True)
    app.run_server(host='0.0.0.0',debug=True, port=8050)