from pymongo import MongoClient

def show_dbs():
    db_list = []
    client = MongoClient()
    db = client.temp_database
    colls = db.collection_names()
    
    for dbs in colls:
        if dbs != 'boton_enable' and dbs != 'nombre_db_en_uso' and dbs != 'ts_mi_db_in' and dbs != 'omega':
            db_list.append(dbs)

    return db_list