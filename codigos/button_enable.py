import json
from pymongo import MongoClient

def boton(ToF):

    client = MongoClient()		
    db = client.temp_database
    mqtt_collection = db.boton_enable

    mqtt_collection.drop()

    button_state = {
        "Button disabled": ToF,
    }
    estado_boton = mqtt_collection.insert_one(button_state)

    return

def estado():

    boton_state = False

    client = MongoClient()		
    db = client.temp_database	
    mqtt_collection = db.boton_enable	

    estado = mqtt_collection.find()

    for state in estado:

        boton_state = state['Button disabled']

    return boton_state 

def db_en_uso(db_en_uso):

    client = MongoClient()		
    db = client.temp_database
    mqtt_collection = db.nombre_db_en_uso

    mqtt_collection.drop()

    dbname = {
        "Nombre de la base de datos en uso": db_en_uso,
    }
    name = mqtt_collection.insert_one(dbname)

    return

def db_actual():

    midb = ''

    client = MongoClient()		
    db = client.temp_database	
    mqtt_collection = db.nombre_db_en_uso	

    nombre = mqtt_collection.find()

    for nb in nombre:

        midb = nb['Nombre de la base de datos en uso']

    return midb 