import datetime
import time
import json
import paho.mqtt.client as mqtt
import sys
from pymongo import MongoClient
from bson import json_util
from datetime import date
from datetime import datetime

def create_db_in():

    client = MongoClient()		
    db = client.temp_database
    mqtt_collection = db.ts_mi_db_in
    
    mqtt_collection.drop()

    today = date.today()
    d1 = today.strftime("%Y/%m/%d") 
    now = datetime.now()
    dt_string = now.strftime("%H:%M:%S") 
    fecha_in = d1 + " " + dt_string,

    tiempo = {
        "Fecha de inicio de la base de datos": fecha_in
    }

    stored_time = mqtt_collection.insert_one(tiempo)

    return 

# def create_db_fin():

#     client = MongoClient()		
#     db = client.temp_database
#     mqtt_collection = db.ts_mi_db_fin
    
#     mqtt_collection.drop()

#     today = date.today()
#     d1 = today.strftime("%Y/%m/%d") 
#     now = datetime.now()
#     dt_string = now.strftime("%H:%M:%S") 
#     fecha_fin = d1 + " " + dt_string

#     inicio_collection = db.ts_mi_db_in
#     inicio = inicio_collection.find()

#     for item in inicio:

#         t_inicio = item['Fecha de inicio de la base de datos']

#     midb = db.nombre_db_en_uso

#     nombre_db = midb.find()

#     for name in nombre_db:

#         nombre_database = name['Nombre de la base de datos en uso']

#     nombre_database = db[nombre_database]

#     posts_collection = db.omega
 
#     for post in posts_collection.find({"Fecha y hora": {"$gte": t_inicio[0], "$lt": fecha_fin}}):

#             tiempo = post['Fecha y hora']
#             temperatura = (float(post['Temperatura']))

#             medicion = {
#             "Fecha y hora": tiempo,z
#             "Temperatura": temperatura,
#             }
#             stored_med = nombre_database.insert_one(medicion) 
  
#     return 


def create_db_fin():

    client = MongoClient()		
    db = client.temp_database
    mqtt_collection = db.ts_mi_db_fin
    
    mqtt_collection.drop()

    today = date.today()
    d1 = today.strftime("%Y/%m/%d") 
    now = datetime.now()
    dt_string = now.strftime("%H:%M:%S") 
    fecha_fin = d1 + " " + dt_string

    inicio_collection = db.ts_mi_db_in
    inicio = inicio_collection.find()

    for item in inicio:

        t_inicio = item['Fecha de inicio de la base de datos']

    midb = db.nombre_db_en_uso

    nombre_db = midb.find()

    for name in nombre_db:

        nombre_database = name['Nombre de la base de datos en uso']

    nombre_database = db[nombre_database]

    posts_collection = db.omega

    asd = 0
 
    for post in posts_collection.find({"Fecha y hora": {"$gte": t_inicio[0], "$lt": fecha_fin}}):

            tiempo = post['Fecha y hora']
            temperatura = (float(post['Temperatura']))

            medicion = {
            "Fecha y hora": asd,
            "Temperatura": temperatura,
            }
            stored_med = nombre_database.insert_one(medicion) 

            asd = asd + 1
  
    return 