import paho.mqtt.client as mqtt
import time
import os

# MQTT Settings 
MQTT_Broker = "test.mosquitto.org"
MQTT_Port = 1883
Keep_Alive_Interval = 45
MQTT_Topic = "Sensor/Temperatura"

class mqttsub():
    def __init__(self):
        self.temp = 0

    #Subscribe to all Sensors at Base Topic
    def on_connect(self, mqttc, mosq, obj, rc):
        mqttc.subscribe(MQTT_Topic, 0)

    def on_message(self, mosq, obj, msg):
        self.temp = msg.payload.decode('utf-8')

    def on_subscribe(self, mosq, obj, mid, granted_qos):
        pass

def get_temp():

    mqttc = mqtt.Client()
    # Assign event callbacks
    mqttobj = mqttsub()
    mqttc.on_message = mqttobj.on_message
    mqttc.on_connect = mqttobj.on_connect

    # Connect
    mqttc.connect(MQTT_Broker, int(MQTT_Port), int(Keep_Alive_Interval))

    mqttc.loop_start()
    time.sleep(2)

    # Continue the network loop
    mqttc.loop_stop()

    return mqttobj.temp
