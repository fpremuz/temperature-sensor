import json
from pymongo import MongoClient
from bson import json_util
from datetime import datetime, timezone
from datetime import timedelta
import time

def graph_mqtt(t, frec):

    if frec is None:
        frec = 0.02

    frecuencia = frec * 60

    mqtt_time = []
    mqtt_temp = []
    tiempo_list = [0]

    now = datetime.now()
    tiempo = now.strftime("%Y/%m/%d %H:%M:%S")
    ahora = datetime.timestamp(now)

    timespan = 3600 * t + 0.001

    para_antes = time.mktime(datetime.strptime(tiempo, "%Y/%m/%d %H:%M:%S").timetuple())
    antes = para_antes - timespan
    antes2 = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(antes))

    client = MongoClient()		
    db = client.temp_database	
    mqtt_collection = db.omega   

    for muestra in mqtt_collection.find({"Fecha y hora": {"$gte": antes2}}):

        tmp = muestra['Fecha y hora']
        ts_tiempo = time.mktime(datetime.strptime(tmp, "%Y/%m/%d %H:%M:%S").timetuple()) 

        if ts_tiempo - tiempo_list[-1] >= frecuencia:    
            if tiempo_list[0] == 0:
                tiempo_list.pop(0)
            tiempo_list.append(ts_tiempo) 
            mqtt_time.append(muestra['Fecha y hora'])
            mqtt_temp.append(float(muestra['Temperatura']))
    
    return mqtt_time, mqtt_temp    