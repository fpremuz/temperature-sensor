import datetime
import json
import paho.mqtt.client as mqtt
import sys
from pymongo import MongoClient
from bson import json_util
from datetime import date
from datetime import datetime

MQTT_Broker = "test.mosquitto.org"	# the web address that will publish the MQTT data
MQTT_Port = 1883		# web port to access MQTT_Broker
Keep_Alive_Interval = 45	# number of seconds to keep the connection alive between pings
MQTT_Topic = "Sensor/Temperatura" 

#if len(sys.argv) > 2 : MQTT_Topic = sys.argv[1] # if the user passed a parameter to the script, use the parameter as the topic instead.

# MongoDB settings
client = MongoClient()		# connect to the local Mongo client
db = client.temp_database	# connect to the temp_database
mqtt_collection = db.omega	# connect to the temp_database.temp_collection

# MQTT Methods: Subscribes to the specified topic (MQTT_Topic)
def on_connect(mqttc, mosq, obj, rc):
    mqttc.subscribe(MQTT_Topic, 0)

# When the message is received, it is processed and stored in the database. This is the Master Call for saving MQTT Data into DB
def on_message(mosq, obj, msg):
    today = date.today()
    d1 = today.strftime("%Y/%m/%d") #fecha en formato aaaa/mm/dd
    now = datetime.now()
    dt_string = now.strftime("%H:%M:%S") #now.strftime("%H:%M%S") hora en hora:minutos:segundos
    print("Temperatura recibida: " + msg.payload.decode('utf-8') + "ºC, en el topic '" + msg.topic + "'" + " A las " + dt_string + " hs")
    print("Almacenando información...")
    #try:
    medicion = {
        "Temperatura": msg.payload.decode('utf-8'),
        #"Fecha": d1,
        #"Hora": dt_string,
        "Fecha y hora": d1 + " " + dt_string,
    }
    # Insert Data
    stored_med = mqtt_collection.insert_one(medicion)
    print("Se insertó la siguiente medición:", stored_med)
  
    # Printing the data inserted
    cursor = mqtt_collection.find()
    for record in cursor:
        print(record)

# unused method
def on_subscribe(mosq, obj, mid, granted_qos):
    pass

# Script Begins:
mqttc = mqtt.Client()
print("conectandose...")
# Assign event callbacks
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.subscribe("#")
print("suscripto")
# Connect
mqttc.connect(MQTT_Broker, int(MQTT_Port), int(Keep_Alive_Interval))
print("conectado")
# Continue the network loop
mqttc.loop_forever()