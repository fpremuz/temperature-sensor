from pymongo import MongoClient

def borrar_database(opcion):

    client = MongoClient()		
    db = client.temp_database	
    mqtt_collection = db[opcion]	

    mqtt_collection.drop()

    return 