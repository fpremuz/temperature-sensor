from pymongo import MongoClient
from datetime import datetime
import time

# def ejes_grafico(opcion, frec):

#     frecuencia = frec * 60

#     time_list = []
#     temp_list = []
#     tiempo = [0]

#     client = MongoClient()		
#     db = client.temp_database	
#     mqtt_collection = db[opcion]	

#     database = mqtt_collection.find()

#     for item in database:

#         tmp = item['Fecha y hora']
#         ts_tiempo = time.mktime(datetime.strptime(tmp, "%Y/%m/%d %H:%M:%S").timetuple()) 
        
#         if ts_tiempo - tiempo[-1] >= frecuencia:    
#             if tiempo[0] == 0:
#                 tiempo.pop(0)
#             tiempo.append(ts_tiempo) 
#             time_list.append(item['Fecha y hora'])
#             temp_list.append(float(item['Temperatura']))

#     return time_list, temp_list


def ejes_grafico(opcion, frec):

    frecuencia = frec * 60

    time_list = []
    temp_list = []
    tiempo = [0]

    client = MongoClient()		
    db = client.temp_database	
    mqtt_collection = db[opcion]	

    database = mqtt_collection.find()

    for item in database:

        tmp = float(item['Fecha y hora'])
        
        if tmp - tiempo[-1] >= frecuencia:    
            tiempo.append(tmp) 
            if frec < 1:
                tmp = tmp
            elif 1 <= frec < 30:
                tmp = tmp / 60
            elif frec >= 30:
                tmp = tmp / 3600
            time_list.append(tmp)
            temp_list.append(float(item['Temperatura']))

    return time_list, temp_list