# IoT Temperature sensor

This project consists of a temperature sensor connected to a microcontroller which uses Wi-Fi connection to send sensor data to a server, stores it, and shows it in real time in a website.

The sensor (DS18b20), which will be used in a bio reactor, is connected to a ESP32 and transfers the data (which is digitalized by the sensor) via Wi-Fi (Wi-Fi manager library: https://github.com/tzapu/WiFiManager) using the MQTT protocol. This data is received in an Oracle Cloud linux virtual machine free instance, and is stored every second in MongoDB (using mqttmongo.py code).

Using dash plotly, a dashboard was created to show a graph with the sensor readings in real time, and also lets us see a graph of previously saved measurements. It is possible to download these collections from the database. The user can choose the range of time to see in the real time graph, and also the time between samples. The website is hosted in OCI as well.

![Screenshot_from_2021-08-29_16-09-05](/uploads/614eef905e4e69e5f8e647e66b41676b/Screenshot_from_2021-08-29_16-09-05.png)

When the device is not plugged in, the temperature shows 0 and it is not stored.
To the left of the screen we see a sidebar that has many options. 
* The first dropdown menu box ("Graficar base de datos") lets the user graph previously stored measurements, each one in a different graph that appears below the main iot graph.
* The second is an input field which allows the user to save a collection in the database with the name in the input field (these are the ones plotted when selected in the fist field). When "Comenzar a guardar datos" blue box is clicked, the samples begin to be stored in a collection with the chosen name. Immediately, the button is disabled and the next button ("Finalizar guardado de datos") is enabled. This last one is to finish the measurement. Once clicked, this collection will be availeble to plot in the first dropdown.
* The following field is another dropdown, which lets the user pick one previously saved database and download it (by clicking the grey "Descargar base de datos" button) as a .csv file
* The dropdown below performs in a similar way but deletes the collection instead.
* The next field only allows positive numbers as input and is used to select the time between two samples, being 0 the minimum (adopts 0.02 instead, which is 1 second).
* Finally, the main graph has a similar field that is used to chose its timespan. The default value is 24 hours, which means in the live plot the user will see the temperature of the last 24 hours.
